import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hives/model/main.dart';

class MainStore extends ChangeNotifier {
  Box box = Hive.box('root');
  String names = '';
  DateTime dates = DateTime.now();

  void nameAdd(val) {
    names = val;
    notifyListeners();
  }

  void dateAdd(val) {
    dates = val;
    notifyListeners();
  }

  void addtoDB() {
    BasicDatabase data = BasicDatabase(name: names, date: dates);
    box.add(data);
    names = '';
    dates = DateTime.now();
    notifyListeners();
  }

  void removefromDB(int index) {
    box.deleteAt(index);
    notifyListeners();
  }

  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }
}
