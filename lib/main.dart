import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hives/model/main.dart';
import 'package:hives/store/main.dart';
import 'package:provider/provider.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(BasicDatabaseAdapter());
  await Hive.openBox('root');

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<MainStore>(create: (_) => MainStore())
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MainStore>(
      builder: ((context, value, child) {
        return Scaffold(
          body: ValueListenableBuilder(
            valueListenable: Hive.box('root').listenable(),
            builder: (context, list, child) {
              if (list.isEmpty) {
                return const Center(
                  child: Text('No data found.'),
                );
              }
              return ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onLongPress: () {
                      showDialog(
                        context: context,
                        builder: (contexts) {
                          return SimpleDialog(
                            title: const Center(
                              child: Text('Are you sure you want to delete?'),
                            ),
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 30),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 100,
                                      height: 50,
                                      child: OutlinedButton(
                                        onPressed: () {
                                          value.removefromDB(index);
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text('Yes'),
                                      ),
                                    ),
                                    const Expanded(child: SizedBox()),
                                    SizedBox(
                                      width: 100,
                                      height: 50,
                                      child: OutlinedButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text('No'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: ListTile(
                      title: Text(list.getAt(index).name == ''
                          ? '(undefined)'
                          : list.getAt(index).name),
                      subtitle: Text(
                          (list.getAt(index).date).toString().split('.')[0]),
                    ),
                  );
                },
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return SimpleDialog(
                    contentPadding: const EdgeInsets.all(10),
                    children: [
                      TextField(
                        decoration: const InputDecoration(label: Text('Name')),
                        controller: _controller,
                        onChanged: (text) {
                          value.nameAdd(text);
                        },
                        onSubmitted: (x) {
                          value.addtoDB();
                          _controller.clear();
                          Navigator.of(context).pop();
                        },
                      ),
                      ElevatedButton(
                          onPressed: () {
                            value.addtoDB();
                            _controller.clear();
                            Navigator.of(context).pop();
                          },
                          child: const Text('Add'))
                    ],
                  );
                },
              ).then((x) => _controller.clear());
            },
          ),
        );
      }),
    );
  }
}
