import 'package:hive/hive.dart';

part 'main.g.dart';

@HiveType(typeId: 1)
class BasicDatabase {
  BasicDatabase({required this.name, required this.date});

  @HiveField(0)
  final String name;

  @HiveField(1)
  final DateTime date;
}
